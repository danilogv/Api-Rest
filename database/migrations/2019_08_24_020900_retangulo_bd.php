<?php

    use Illuminate\Support\Facades\Schema;
    use Illuminate\Database\Schema\Blueprint;
    use Illuminate\Database\Migrations\Migration;

    class RetanguloBd extends Migration
    {
        public function up()
        {
            Schema::create('Retangulo', function (Blueprint $table) {
                $table->bigIncrements("id");
                $table->decimal("base",10,3)->nullable(false);
                $table->decimal("altura",10,3)->nullable(false);
            });
        }

        public function down()
        {
            Schema::dropIfExists('Retangulo');
        }
    }

?>

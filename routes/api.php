<?php

    Route::post("retangulo", ["uses" => "CadastroRetangulo@post"]);
    Route::post("triangulo", ["uses" => "CadastroTriangulo@post"]);
    Route::get("retangulo/{id}",["uses" => "BuscaRetangulo@get"]);
    Route::get("triangulo/{id}",["uses" => "BuscaTriangulo@get"]);
    Route::delete("retangulo/{id}",["uses" => "RemoveRetangulo@delete"]);
    Route::delete("triangulo/{id}",["uses" => "RemoveTriangulo@delete"]);
    Route::put("retangulo/{id}",["uses" => "AlteraRetangulo@put"]);
    Route::put("triangulo/{id}",["uses" => "AlteraTriangulo@put"]);
    Route::get("soma-areas", ["uses" => "Area@get"]);

?>

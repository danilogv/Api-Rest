<?php

    namespace App\Models;

    use Illuminate\Database\Eloquent\Model;
    use Illuminate\Support\Facades\DB;
    use App\Http\Controllers\Http as Http;

    class Triangulo extends Model
    {

        protected $table = "triangulo";
        protected $fillable = ["base","altura"];
        public $timestamps = false;

        public function insereTriangulo($dados)
        {
            DB::beginTransaction();
            try {
                self::create($dados);
                DB::commit();
            } catch(Exception $ex){
                DB::rollback();
                return Http::SERVER_ERROR;
            }
            return Http::CREATED;
        }

        public function alteraTriangulo($id,$base,$altura)
        {
            $triangulo = self::find($id);
            if($triangulo != null){
                try {
                    $triangulo->base = $base;
                    $triangulo->altura = $altura;
                    DB::beginTransaction();
                    $triangulo->save();
                    DB::commit();
                } catch(Exception $ex){
                    DB::rollback();
                    $triangulo->base = -1;
                    $triangulo->altura = -1;
                }
            }
            return $triangulo;
        }

        public function removeTriangulo($id)
        {
            try {
                $triangulo = self::find($id);
                if($triangulo == null){
                    return Http::UNPROCESSABLE;
                } else {
                    DB::beginTransaction();
                    self::destroy($id);
                    DB::commit();
                }
            } catch(Exception $ex){
                DB::rollback();
                return Http::SERVER_ERROR;
            }
            return Http::NO_CONTENT;
        }

        public function getTriangulo($id)
        {
            return self::find($id);
        }

        public function getTriangulos()
        {
            return self::get();
        }

    }

?>

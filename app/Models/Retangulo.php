<?php

    namespace App\Models;

    use Illuminate\Database\Eloquent\Model;
    use Illuminate\Support\Facades\DB;
    use App\Http\Controllers\Http as Http;

    class Retangulo extends Model
    {

        protected $table = "retangulo";
        protected $fillable = ["base","altura"];
        public $timestamps = false;

        public function insereRetangulo($dados)
        {
            DB::beginTransaction();
            try {
                self::create($dados);
                DB::commit();
            } catch(Exception $ex){
                DB::rollback();
                return Http::SERVER_ERROR;
            }
            return Http::CREATED;
        }

        public function alteraRetangulo($id,$base,$altura)
        {
            $retangulo = self::find($id);
            if($retangulo != null){
                try {
                    $retangulo->base = $base;
                    $retangulo->altura = $altura;
                    DB::beginTransaction();
                    $retangulo->save();
                    DB::commit();
                } catch(Exception $ex){
                    DB::rollback();
                    $retangulo->base = -1;
                    $retangulo->altura = -1;
                }
            }
            return $retangulo;
        }

        public function removeRetangulo($id)
        {
            try {
                $retangulo= self::find($id);
                if($retangulo == null){
                    return Http::UNPROCESSABLE;
                } else {
                    DB::beginTransaction();
                    self::destroy($id);
                    DB::commit();
                }
            } catch(Exception $ex){
                DB::rollback();
                return Http::SERVER_ERROR;
            }
            return Http::NO_CONTENT;
        }

        public function getRetangulo($id)
        {
            return self::find($id);
        }

        public function getRetangulos()
        {
            return self::get();
        }

    }

?>

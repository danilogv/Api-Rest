<?php

    namespace App\Http\Controllers;

    class Http
    {

        public const OK = 200;
        public const CREATED = 201;
        public const NO_CONTENT = 204;
        public const UNPROCESSABLE = 422;
        public const SERVER_ERROR = 500;
        
    }

?>

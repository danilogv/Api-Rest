<?php

    namespace App\Http\Controllers;

    use Illuminate\Routing\Controller as BaseController;
    use App\Models\Retangulo as Retangulo;
    use App\Http\Controllers\Http as Http;

    class RemoveRetangulo extends BaseController
    {

        private $rt;
        private $msg;

        public function __construct()
        {
            $this->rt = new Retangulo();
            $this->msg = null;
        }

        public function delete($id)
        {
            if(!is_numeric($id)){
                $this->msg = "O id do retângulo deve ser um número inteiro !";
                return response()->json(["mensagem" => $this->msg],Http::UNPROCESSABLE);
            }
            $http = $this->rt->removeRetangulo($id);
            if($http == Http::UNPROCESSABLE){
                $this->msg = "Retângulo não cadastrado !";
                return response()->json($this->msg,$http);
            } else if($http == Http::SERVER_ERROR){
                $this->msg = "Problemas no BD !";
                return response()->json($this->msg,$http);
            }
            return response()->json(null,$http);
        }
    }

?>

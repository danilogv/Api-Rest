<?php

    namespace App\Http\Controllers;

    use Illuminate\Routing\Controller as BaseController;
    use Illuminate\Http\Request;
    use App\Models\Triangulo as Triangulo;
    use App\Http\Controllers\Http as Http;

    class CadastroTriangulo extends BaseController
    {

        private $tg;
        private $msg;

        public function __construct()
        {
            $this->tg = new Triangulo();
            $this->msg = null;
        }

        public function post(Request $req)
        {
            $base = $req->input("base");
            $altura = $req->input("altura");
            if(!is_numeric($base) || !is_numeric($altura)){
                $this->msg = "A base e a altura devem ser valores numéricos !";
                return response()->json(["mensagem" => $this->msg],Http::UNPROCESSABLE);
            } else if($base < 0 || $altura < 0){
                $this->msg = "A base e a altura devem ser números positivos !";
                return response()->json(["mensagem" => $this->msg],Http::UNPROCESSABLE);
            }
            $dados = ["base" => $base, "altura" => $altura];
            $http = $this->tg->insereTriangulo($dados);
            if($http == Http::SERVER_ERROR){
                $this->msg = "Problemas no BD !";
                return response()->json(["mensagem" => $this->msg],$http);
            }
            return response()->json($dados,$http);
        }

    }

?>

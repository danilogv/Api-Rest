<?php

    namespace App\Http\Controllers;

    use Illuminate\Routing\Controller as BaseController;
    use App\Models\Triangulo as Triangulo;
    use App\Http\Controllers\Http as Http;

    class RemoveTriangulo extends BaseController
    {

        private $tg;
        private $msg;

        public function __construct()
        {
            $this->tg = new Triangulo();
            $this->msg = null;
        }

        public function delete($id)
        {
            if(!is_numeric($id)){
                $msg = "O id do triângulo deve ser um número inteiro !";
                return response()->json(["mensagem" => $msg],Http::UNPROCESSABLE);
            }
            $http = $this->tg->removeTriangulo($id);
            if($http == Http::UNPROCESSABLE){
                $this->msg = "Triângulo não cadastrado !";
                return response()->json($this->msg,$http);
            } else if($http == Http::SERVER_ERROR){
                $this->msg = "Problemas no BD !";
                return response()->json($this->msg,$http);
            }
            return response()->json(null,$http);
        }

    }

?>

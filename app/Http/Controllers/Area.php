<?php

    namespace App\Http\Controllers;

    use Illuminate\Routing\Controller as BaseController;
    use App\Models\Retangulo as Retangulo;
    use App\Models\Triangulo as Triangulo;
    use App\Http\Controllers\Http as Http;

    class Area extends BaseController
    {

        private $rt;
        private $tg;

        public function __construct()
        {
            $this->rt = new Retangulo();
            $this->tg = new Triangulo();
        }

        public function get()
        {
            $somaArea = 0;
            $listaRetangulos = $this->rt->getRetangulos();
            for($i = 0;$i < count($listaRetangulos);$i++){
                $somaArea += $listaRetangulos[$i]->base * $listaRetangulos[$i]->altura;
            }
            $listaTriangulos = $this->tg->getTriangulos();
            for($i = 0;$i < count($listaTriangulos);$i++){
                $somaArea += ($listaTriangulos[$i]->base * $listaTriangulos[$i]->altura) / 2;
            }
            return response()->json(["mensagem" => $somaArea],Http::OK);
        }

    }

?>

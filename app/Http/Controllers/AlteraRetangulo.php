<?php

    namespace App\Http\Controllers;

    use Illuminate\Routing\Controller as BaseController;
    use Illuminate\Http\Request;
    use App\Models\Retangulo as Retangulo;
    use App\Http\Controllers\Http as Http;

    class AlteraRetangulo extends BaseController
    {

        private $rt;
        private $msg;

        public function __construct()
        {
            $this->rt = new Retangulo();
            $this->msg = null;
        }

        public function put(Request $req,$id)
        {
            $base = $req->input("base");
            $altura = $req->input("altura");
            if(!is_numeric($base) || !is_numeric($altura)){
                $this->msg = "A base e a altura devem ser valores numéricos !";
                return response()->json(["mensagem" => $this->msg],Http::UNPROCESSABLE);
            } else if($base < 0 || $altura < 0){
                $this->msg = "A base e a altura devem ser números positivos !";
                return response()->json(["mensagem" => $this->msg],Http::UNPROCESSABLE);
            }
            $retangulo = $this->rt->alteraRetangulo($id,$base,$altura);
            if($retangulo == null){
                $this->msg = "Retângulo não cadastrado !";
                return response()->json(["mensagem" => $this->msg],Http::UNPROCESSABLE);
            } else if($retangulo->base == -1 && $retangulo->altura == - 1){
                $this->msg = "Problemas no BD !";
                return response()->json(["mensagem" => $this->msg],Http::SERVER_ERROR);
            }
            return response()->json($retangulo,Http::OK);
        }

    }

?>

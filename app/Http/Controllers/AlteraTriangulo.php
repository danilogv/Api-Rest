<?php

    namespace App\Http\Controllers;

    use Illuminate\Routing\Controller as BaseController;
    use Illuminate\Http\Request;
    use App\Models\Triangulo as Triangulo;
    use App\Http\Controllers\Http as Http;

    class AlteraTriangulo extends BaseController
    {

        private $tg;
        private $msg;

        public function __construct()
        {
            $this->tg = new Triangulo();
            $this->msg = null;
        }

        public function put(Request $req,$id)
        {
            $base = $req->input("base");
            $altura = $req->input("altura");
            if(!is_numeric($base) || !is_numeric($altura)){
                $this->msg = "A base e a altura devem ser valores numéricos !";
                return response()->json(["mensagem" => $this->msg],Http::UNPROCESSABLE);
            } else if($base < 0 || $altura < 0){
                $this->msg = "A base e a altura devem ser números positivos !";
                return response()->json(["mensagem" => $this->msg],Http::UNPROCESSABLE);
            }
            $triangulo = $this->tg->alteraTriangulo($id,$base,$altura);
            if($triangulo == null){
                $this->msg = "Triângulo não cadastrado !";
                return response()->json(["mensagem" => $this->msg],Http::UNPROCESSABLE);
            } else if($triangulo->base == -1 && $triangulo->altura == - 1){
                $this->msg = "Problemas no BD !";
                return response()->json(["mensagem" => $this->msg],Http::SERVER_ERROR);
            }
            return response()->json($triangulo,Http::OK);
        }

    }

?>

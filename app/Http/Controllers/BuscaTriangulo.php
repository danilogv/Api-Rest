<?php

    namespace App\Http\Controllers;

    use Illuminate\Routing\Controller as BaseController;
    use App\Models\Triangulo as Triangulo;
    use App\Http\Controllers\Http as Http;

    class BuscaTriangulo extends BaseController
    {

        private $tg;
        private $msg;

        public function __construct()
        {
            $this->tg = new Triangulo();
            $this->msg = null;
        }

        public function get($id)
        {
            if(!is_numeric($id)){
                $this->msg = "O id do triângulo deve ser um número inteiro !";
                return response()->json(["mensagem" => $this->msg],Http::UNPROCESSABLE);
            }
            $triangulo = $this->tg->getTriangulo($id);
            if($triangulo == null){
                $this->msg = "A figura com esse código não foi cadastrada !";
                return response()->json(["mensagem" => $this->msg],Http::UNPROCESSABLE);
            }
            return response()->json($triangulo,Http::OK);
        }

    }

?>

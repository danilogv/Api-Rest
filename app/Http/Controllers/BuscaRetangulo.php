<?php

    namespace App\Http\Controllers;

    use Illuminate\Routing\Controller as BaseController;
    use App\Models\Retangulo as Retangulo;
    use App\Http\Controllers\Http as Http;

    class BuscaRetangulo extends BaseController
    {

        private $rt;
        private $msg;

        public function __construct()
        {
            $this->rt = new Retangulo();
            $this->msg = null;
        }

        public function get($id)
        {
            if(!is_numeric($id)){
                $this->msg = "O id do retângulo deve ser um número inteiro !";
                return response()->json(["mensagem" => $this->msg],Http::UNPROCESSABLE);
            }
            $retangulo = $this->rt->getRetangulo($id);
            if($retangulo == null){
                $this->msg = "A figura com esse código não foi cadastrada !";
                return response()->json(["mensagem" => $this->msg],Http::UNPROCESSABLE);
            }
            return response()->json($retangulo,Http::OK);
        }

    }

?>
